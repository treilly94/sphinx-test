"""
main.py
====================================
A demo module for this project
"""


def example_func(param1):
    """
    Return the most important thing about a person.
    Parameters

    param1
        A string parameter.
    """
    return "The wise {} loves Python.".format(param1)


class ExampleClass:
    """An example docstring for a class definition."""

    def __init__(self, name):
        """
        Blah blah blah.
        Parameters
        ---------
        name
            A string to assign to the `name` instance attribute.
        """
        self.name = name

    def about_self(self):
        """
        Return information about an instance created from ExampleClass.
        """
        return "I am a very smart {} object.".format(self.name)
