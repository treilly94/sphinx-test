.. sphinx-test documentation master file, created by
   sphinx-quickstart on Thu Jan  2 18:54:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx-test's documentation!
=======================================

This is a project I made to test out creating a wiki using `sphinx <https://www.sphinx-doc.org>`_ and publishing it using `gitlab pages <https://docs.gitlab.com/ce/user/project/pages/>`_

Documentation
-------------

This is the documentation for our project or something

.. toctree::
   :maxdepth: 2

   design/index
   tools/index

API Reference
-------------

If you are looking for information on a specific function, class, or
method, this part of the documentation is for you.

.. toctree::
   :maxdepth: 2

   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
